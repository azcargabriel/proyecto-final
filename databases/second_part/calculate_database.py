# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
import scipy.stats as sc

def calculate_and_write(f_in, f_out, class_type, features, k):
    #Promedios
    if 'mean' in features:
        i = 0
        means = []
        while(i < k):
            m1 = (f_in.values[:, i]).mean()
            means.append(m1)
            i = i + 1
        f_out.write(str(means)[1:-1] + ", ")
    
    #Desviacion estandar
    if 'std' in features:
        i = 0
        std = []
        while(i < k):
            m2 = (f_in.values[:, i]).std()
            std.append(m2)
            i = i + 1
        f_out.write(str(std)[1:-1] + ", ")
        
    #Rango
    if 'range' in features:
        i = 0
        ran = []
        while(i < k):
            m3 = (f_in.values[:, i]).ptp()
            ran.append(m3)
            i = i + 1
        f_out.write(str(ran)[1:-1] + ", ")
    
    #Varianza'var',
    if 'var' in features:
        i = 0
        v = []
        while(i < k):
            m4 = (f_in.values[:, i]).var()
            v.append(m4)
            i = i + 1
        f_out.write(str(v)[1:-1] + ", ")        
  
    #Max
    if 'max' in features:
        i = 0
        mx = []
        while(i < k):
            m6 = np.amax((f_in.values[:, i]))
            mx.append(m6)
            i = i + 1
        f_out.write(str(mx)[1:-1] + ", ")
        
    #Min
    if 'min' in features:
        i = 0
        mn = []
        while(i < k):
            m7 = np.amin((f_in.values[:, i]))
            mn.append(m7)
            i = i + 1
        f_out.write(str(mn)[1:-1] + ", ")
        
    #Entropy
    if 'entropy' in features:
        i = 0
        ent = []
        while(i < k):
            data = f_in.values[:, i]
            p_data= (pd.Series(data)).value_counts()/len(data)
            m8 = sc.entropy(p_data)
            ent.append(m8)
            i = i + 1
        f_out.write(str(ent)[1:-1] + ", ")
        
    f_out.write(str(class_type) + "\n")
def main(features, db_type, k):
    db = open('database_'+db_type+'.csv','w')
    
    #Bike
    path = "../"+db_type+"/bike"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 0, features, k)
        
    #Climbing
    path = "../"+db_type+"/climbing"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 1, features, k)
            
    #Descending
    path = "../"+db_type+"/descending"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 2, features, k)
        
    #Gymbike
    path = "../"+db_type+"/gymbike"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 3, features, k)
        
    #Jumping
    path = "../"+db_type+"/jumping"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 4, features, k) 
        
    #Running
    path = "../"+db_type+"/running"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 5, features, k)
        
    #Standing
    path = "../"+db_type+"/standing"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 6, features, k)
        
    #Treadmill
    path = "../"+db_type+"/treadmill"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 7, features, k)
        
    #Walking
    path = "../"+db_type+"/walking"
    for filename in os.listdir(path):
        if filename[0] != '.':
            f = pd.read_csv(path+"/"+filename, header=None)
            calculate_and_write(f, db, 8, features, k)
        
    db.close()
    
    db_format = open('db_format_'+db_type+'.txt','w')
    
    #Format
    initial_format = ['Acc_x', 'Acc_y', 'Acc_z', 'Gyr_x', 'Gyr_y', 'Gyr_z', 'Mag_x', 'Mag_y', 'Mag_z']
    
    
    for feat in features:
        for form in initial_format:
            db_format.write(feat + "_" + form + "; ")
        
    db_format.close()
