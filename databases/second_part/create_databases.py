# -*- coding: utf-8 -*-

import pandas as pd

def main(db_type):
    val = 0.2
    train = 0.6
    
    f = pd.read_csv("database_"+db_type+".csv", header=None)
    last = f.shape[1] - 1
    total = len(f)
    
    bike = f.loc[f[last] == 0]
    climbing = f.loc[f[last] == 1]
    descending = f.loc[f[last] == 2]
    gymbike = f.loc[f[last] == 3]
    jumping = f.loc[f[last] == 4]
    running = f.loc[f[last] == 5]
    standing = f.loc[f[last] == 6]
    treadmill = f.loc[f[last] == 7]
    walking = f.loc[f[last] == 8]
    
    percents = [len(bike)/total, len(climbing)/total, len(descending)/total,
                len(gymbike)/total, len(jumping)/total, len(running)/total,
                len(standing)/total, len(treadmill)/total, len(walking)/total]
    
    print("Validación y prueba tendran: " + str(int(val*total)) + " elementos, donde")
    print(str(int(percents[0]*val*total)) + " serán bike")
    print(str(int(percents[1]*val*total)) + " serán climbing")
    print(str(int(percents[2]*val*total)) + " serán descending")
    print(str(int(percents[3]*val*total)) + " serán gymbike")
    print(str(int(percents[4]*val*total)) + " serán jumping")
    print(str(int(percents[5]*val*total)) + " serán running")
    print(str(int(percents[6]*val*total)) + " serán standing")
    print(str(int(percents[7]*val*total)) + " serán treadmill")
    print(str(int(percents[8]*val*total)) + " serán walking\n")
    
    print("Entrenamiento tendrá: " + str(int(train*total)) + " elementos")
    print(str(int(percents[0]*train*total)) + " serán bike")
    print(str(int(percents[1]*train*total)) + " serán climbing")
    print(str(int(percents[2]*train*total)) + " serán descending")
    print(str(int(percents[3]*train*total)) + " serán gymbike")
    print(str(int(percents[4]*train*total)) + " serán jumping")
    print(str(int(percents[5]*train*total)) + " serán running")
    print(str(int(percents[6]*train*total)) + " serán standing")
    print(str(int(percents[7]*train*total)) + " serán treadmill")
    print(str(int(percents[8]*train*total)) + " serán walking\n")
    
    
    test_db = pd.concat([bike.iloc[0:int(percents[0]*val*total), :],
                         climbing.iloc[0:int(percents[1]*val*total), :],
                         descending.iloc[0:int(percents[2]*val*total), :],
                         gymbike.iloc[0:int(percents[3]*val*total), :],
                         jumping.iloc[0:int(percents[4]*val*total), :],
                         running.iloc[0:int(percents[5]*val*total), :],
                         standing.iloc[0:int(percents[6]*val*total), :],
                         treadmill.iloc[0:int(percents[7]*val*total), :],
                         walking.iloc[0:int(percents[8]*val*total), :]
                         ])
    
    validation_db = pd.concat([bike.iloc[int(percents[0]*val*total):2*int(percents[0]*val*total), :],
                         climbing.iloc[int(percents[1]*val*total):2*int(percents[1]*val*total), :],
                         descending.iloc[int(percents[2]*val*total):2*int(percents[2]*val*total), :],
                         gymbike.iloc[int(percents[3]*val*total):2*int(percents[3]*val*total), :],
                         jumping.iloc[int(percents[4]*val*total):2*int(percents[4]*val*total), :],
                         running.iloc[int(percents[5]*val*total):2*int(percents[5]*val*total), :],
                         standing.iloc[int(percents[6]*val*total):2*int(percents[6]*val*total), :],
                         treadmill.iloc[int(percents[7]*val*total):2*int(percents[7]*val*total), :],
                         walking.iloc[int(percents[8]*val*total):2*int(percents[8]*val*total), :]
                         ])
    
    training_db = pd.concat([bike.iloc[2*int(percents[0]*val*total):, :],
                         climbing.iloc[2*int(percents[1]*val*total):, :],
                         descending.iloc[2*int(percents[2]*val*total):, :],
                         gymbike.iloc[2*int(percents[3]*val*total):, :],
                         jumping.iloc[2*int(percents[4]*val*total):, :],
                         running.iloc[2*int(percents[5]*val*total):, :],
                         standing.iloc[2*int(percents[6]*val*total):, :],
                         treadmill.iloc[2*int(percents[7]*val*total):, :],
                         walking.iloc[2*int(percents[8]*val*total):, :]
                         ])
    
    test_db.to_csv("validation_db_"+db_type+".csv", header=False, index=False)
    validation_db.to_csv("test_db_"+db_type+".csv", header=False, index=False)
    training_db.to_csv("training_db_"+db_type+".csv", header=False, index=False)