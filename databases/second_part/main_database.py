import calculate_database as ca_d
import create_databases as cr_d
import splitter as sp

use = [0]

#['mean', 'std', 'range', 'var', 'max', 'min', 'entropy']

if 0 in use:
    #Fragmented database
    sp.main(100)
    #ca_d.main(['mean', 'std', 'max'], "fragmented_database", 9)
    
#   Best global 
    ca_d.main(['mean', 'std', 'var', 'max', 'entropy'], "fragmented_database", 9)
    
    cr_d.main("fragmented_database")

if 1 in use:
    #Normal database
    ca_d.main(['mean', 'std', 'var', 'max', 'entropy'], "original_database", 9)
    cr_d.main("original_database")
