# -*- coding: utf-8 -*-
#Particiona las ventanas de tiempo en ventanas mas chicas de a 25 mediciones

import os
import pandas as pd

def splitter(f_in, num, gap, name, path):
    i = 0
    while(i < len(f_in)):
        new = pd.DataFrame(data=f_in.values[i:i+gap,:])
        new.to_csv(path+name+str(num)+".csv", ",", header=False, index=False)
        num = num + 1
        i = i + gap
    return num
         
def main(gap):  
    #Bike
    path = "../original_database/bike/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_bike", "../fragmented_database/bike/")
        
        
    #Climbing
    path = "../original_database/climbing/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_climbing", "../fragmented_database/climbing/")
        
        
    #Descending
    path = "../original_database/descending/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_descending", "../fragmented_database/descending/")
        
        
    #Gymbike
    path = "../original_database/gymbike/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_gymbike", "../fragmented_database/gymbike/")
        
        
    #Jumping
    path = "../original_database/jumping/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_jumping", "../fragmented_database/jumping/")
        
        
    #Running
    path = "../original_database/running/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_running", "../fragmented_database/running/")
        
        
    #Standing
    path = "../original_database/standing/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_standing", "../fragmented_database/standing/")
        
        
    #Treadmill
    path = "../original_database/treadmill/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_treadmill", "../fragmented_database/treadmill/")
        
        
    #Walking
    path = "../original_database/walking/"
    n = 0
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        n = splitter(f, n, gap, "f_walking", "../fragmented_database/walking/")  