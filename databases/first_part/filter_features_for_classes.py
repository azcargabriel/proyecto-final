# -*- coding: utf-8 -*-
import pandas as pd

def main():
    training_db = pd.read_csv("nn_training_db.csv", header=None)
    test_db = pd.read_csv("nn_test_db.csv", header=None)
    
    bike = training_db.loc[training_db[36] == 0]
    climbing = training_db.loc[training_db[36] == 1]
    descending = training_db.loc[training_db[36] == 2]
    
    training_db_1 = pd.concat([bike, climbing, descending])
    
    bike = test_db.loc[test_db[36] == 0]
    climbing = test_db.loc[test_db[36] == 1]
    descending = test_db.loc[test_db[36] == 2]
    
    test_db_1 = pd.concat([bike, climbing, descending])
    
    training_db_1.to_csv("training_db_first_macro.csv", header=False, index=False)
    test_db_1.to_csv("test_db_classes_first_macro.csv", header=False, index=False)
    
    #-----------------------------------------------------------
    
    walking = training_db.loc[training_db[36] == 7]
    treadmill = training_db.loc[training_db[36] == 8]
    
    training_db_1 = pd.concat([walking, treadmill])
    
    walking = test_db.loc[test_db[36] == 7]
    treadmill = test_db.loc[test_db[36] == 8]
    
    test_db_1 = pd.concat([walking, treadmill])
    
    training_db_1.to_csv("training_db_second_macro.csv", header=False, index=False)
    test_db_1.to_csv("test_db_classes_second_macro.csv", header=False, index=False)
