#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from sklearn.utils import shuffle

#Hay: (porcentajes aproximados)
# 30 bikes (7%)
# 45 climbing (12%)
# 45 descending (12%)
# 39 gymbike (10%)
# 45 jumping (12%)
# 45 running (12%)
# 45 standing (12%)
# 44 treadmill (11%)
# 45 walking (12%)
# lo que suma 383 ejemplos

#El de test (20% del total) debe tener 76 ejemplos, de los cuales:
# 5 bikes
# 9 climbing
# 9 descending
# 8 gymbike
# 9 jumping
# 9 running
# 9 standing
# 9 treadmill
# 9 walking

#El de entrenamiento y validación (80% del total) debe tener 307 ejemplos,
#de los cuales (respectivamente):
# 20 y 5 bikes
# 29 y 7 climbing
# 29 y 7 descending
# 25 y 6 gymbike
# 29 y 7 jumping
# 29 y 7 running
# 29 y 7 standing
# 28 y 7 treadmill
# 29 y 7 walking

def main():
    f = pd.read_csv("database.csv", header=None)
    
    bikes = shuffle(f.loc[f[36] == 0])
    climbing = shuffle(f.loc[f[36] == 1])
    descending = shuffle(f.loc[f[36] == 2])
    gymbike = shuffle(f.loc[f[36] == 3])
    jumping = shuffle(f.loc[f[36] == 4])
    running = shuffle(f.loc[f[36] == 5])
    standing = shuffle(f.loc[f[36] == 6])
    treadmill = shuffle(f.loc[f[36] == 7])
    walking = shuffle(f.loc[f[36] == 8])
    
    test_db = pd.concat([bikes.iloc[0:5, :],
                         climbing.iloc[0:9, :],
                         descending.iloc[0:9, :],
                         gymbike.iloc[0:8, :],
                         jumping.iloc[0:9, :],
                         running.iloc[0:9, :],
                         standing.iloc[0:9, :],
                         treadmill.iloc[0:9, :],
                         walking.iloc[0:9, :]
                         ])
    
    validation_db = pd.concat([bikes.iloc[5:10, :],
                         climbing.iloc[9:16, :],
                         descending.iloc[9:16, :],
                         gymbike.iloc[8:14, :],
                         jumping.iloc[9:16, :],
                         running.iloc[9:16, :],
                         standing.iloc[9:16, :],
                         treadmill.iloc[9:16, :],
                         walking.iloc[9:16, :]
                         ])
    
    training_db = pd.concat([bikes.iloc[10:, :],
                         climbing.iloc[16:, :],
                         descending.iloc[16:, :],
                         gymbike.iloc[14:, :],
                         jumping.iloc[16:, :],
                         running.iloc[16:, :],
                         standing.iloc[16:, :],
                         treadmill.iloc[16:, :],
                         walking.iloc[16:, :]
                         ])
    
    
    test_db.to_csv("svm_test_db.csv", header=False, index=False)
    validation_db.to_csv("svm_validation_db.csv", header=False, index=False)
    training_db.to_csv("svm_training_db.csv", header=False, index=False)
