#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from sklearn.utils import shuffle

#Hay: (porcentajes aproximados)
# 30 bikes (7%)
# 45 climbing (12%)
# 45 descending (12%)
# 39 gymbike (10%)
# 45 jumping (12%)
# 45 running (12%)
# 45 standing (12%)
# 44 treadmill (11%)
# 45 walking (12%)
# lo que suma 383 ejemplos

#El de test (20% del total) debe tener 76 ejemplos, de los cuales:
# 5 bikes
# 9 climbing
# 9 descending
# 8 gymbike
# 9 jumping
# 9 running
# 9 standing
# 9 treadmill
# 9 walking

#El de entrenamiento (80% del total) debe tener 307 ejemplos:
# 25 bikes
# 36 climbing
# 36 descending
# 31 gymbike
# 36 jumping
# 36 running
# 36 standing
# 35 treadmill
# 36 walking

def main():
    f = pd.read_csv("database.csv", header=None)
    
    bikes = shuffle(f.loc[f[36] == 0])
    climbing = shuffle(f.loc[f[36] == 1])
    descending = shuffle(f.loc[f[36] == 2])
    gymbike = shuffle(f.loc[f[36] == 3])
    jumping = shuffle(f.loc[f[36] == 4])
    running = shuffle(f.loc[f[36] == 5])
    standing = shuffle(f.loc[f[36] == 6])
    treadmill = shuffle(f.loc[f[36] == 7])
    walking = shuffle(f.loc[f[36] == 8])
    
    test_db = pd.concat([bikes.iloc[0:5, :],
                         climbing.iloc[0:9, :],
                         descending.iloc[0:9, :],
                         gymbike.iloc[0:8, :],
                         jumping.iloc[0:9, :],
                         running.iloc[0:9, :],
                         standing.iloc[0:9, :],
                         treadmill.iloc[0:9, :],
                         walking.iloc[0:9, :]
                         ])
    
    training_db = pd.concat([bikes.iloc[5:, :],
                         climbing.iloc[9:, :],
                         descending.iloc[9:, :],
                         gymbike.iloc[8:, :],
                         jumping.iloc[9:, :],
                         running.iloc[9:, :],
                         standing.iloc[9:, :],
                         treadmill.iloc[9:, :],
                         walking.iloc[9:, :]
                         ])
    
    
    test_db.to_csv("nn_test_db.csv", header=False, index=False)
    training_db.to_csv("nn_training_db.csv", header=False, index=False)
