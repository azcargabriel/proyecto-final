# -*- coding: utf-8 -*-
#Elimina todas las columnas que no estan en el arreglo keep

import pandas as pd
import numpy as np

def main():
    #First Selection
    training_db = pd.read_csv("nn_training_db.csv", header=None)
    test_db = pd.read_csv("nn_test_db.csv", header=None)
    
    keep = np.array([2,4,5,7,8,9,10,11,12,18,19,20,21,24,29,36])
    
    for i in np.linspace(0, 36, num=37):
        if keep.__contains__(int(i)):
            continue
        else:
            training_db.drop(int(i), axis=1, inplace=True)
            test_db.drop(int(i), axis=1, inplace=True)
            
    training_db.to_csv("nn_training_db_first_selection.csv", header=False, index=False)
    test_db.to_csv("nn_test_db_first_selection.csv", header=False, index=False)
    
    
    #Second Selection
    training_db = pd.read_csv("nn_training_db.csv", header=None)
    test_db = pd.read_csv("nn_test_db.csv", header=None)
            
    keep = np.array([20,11,29,21,28,10,30,12,19,22,2,24,13,31,15,33,32,14,23,36])
    
    for i in np.linspace(0, 36, num=37):
        if keep.__contains__(int(i)):
            continue
        else:
            training_db.drop(int(i), axis=1, inplace=True)
            test_db.drop(int(i), axis=1, inplace=True)
            
    training_db.to_csv("nn_training_db_second_selection.csv", header=False, index=False)
    test_db.to_csv("nn_test_db_second_selection.csv", header=False, index=False)
