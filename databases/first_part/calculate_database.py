# -*- coding: utf-8 -*-

import os
import pandas as pd


def calculate_and_write(f_in, f_out, class_type):
    #Promedios
    i = 0
    means = []
    while(i < 9):
        m = (f_in.values[:, i]).mean()
        means.append(m)
        i = i + 1
    
    #Desviacion estandar
    i = 0
    std = []
    while(i < 9):
        m = (f_in.values[:, i]).std()
        std.append(m)
        i = i + 1
        
    #Rango
    i = 0
    ran = []
    while(i < 9):
        m = (f_in.values[:, i]).ptp()
        ran.append(m)
        i = i + 1
    
    #Varianza
    i = 0
    v = []
    while(i < 9):
        m = (f_in.values[:, i]).var()
        v.append(m)
        i = i + 1
        
    f_out.write(str(means)[1:-1] + ", ")
    f_out.write(str(std)[1:-1] + ", ")
    f_out.write(str(ran)[1:-1] + ", ")
    f_out.write(str(v)[1:-1] + ", ")
    f_out.write(str(class_type) + "\n")

def main():
    db = open('database.csv','w')
    
    #Bike
    path = "../original_database/bike/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 0)
        
    #Climbing
    path = "../original_database/climbing/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 1)
        
    #Descending
    path = "../original_database/descending/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 2)
        
    #Gymbike
    path = "../original_database/gymbike/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 3)
        
    #Jumping
    path = "../original_database/jumping/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 4) 
        
    #Running
    path = "../original_database/running/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 5)
        
    #Standing
    path = "../original_database/standing/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 6)
        
    #Treadmill
    path = "../original_database/treadmill/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 7)
        
    #Walking
    path = "../original_database/walking/"
    for filename in os.listdir(path):
        f = pd.read_csv(path+"/"+filename, header=None)
        calculate_and_write(f, db, 8)
        
    db.close()
    
    db_format = open('db_format.txt','w')
    
    #Format
    initial_format = ['Acc_x', 'Acc_y', 'Acc_z', 'Gyr_x', 'Gyr_y', 'Gyr_z', 'Mag_x', 'Mag_y', 'Mag_z']
    #Features
    features = ['mean', 'std', 'range', 'var']
    
    
    for feat in features:
        for form in initial_format:
            db_format.write(feat + "_" + form + "; ")
            
    db_format.close()




