import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt

from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import GaussianNB

def show_results(cm, accuracy, r):    
    fig, ax = plt.subplots()    
    ax.matshow(cm, cmap=plt.cm.Wistia)
    
    for i in range(r):
        for j in range(r):
            c = cm[j,i]
            ax.text(i, j, str(c), va='center', ha='center')
            
    print("\nAccuracy Gamma: " + str(accuracy)+"\n\n")
    
using_clf = [1]

indexes = [11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,35,36,37,38,39,40,41]
    
using_db = 'fragmented_database'

f = pd.read_csv("../databases/second_part/training_db_"+using_db+".csv", header=None)
last = f.shape[1] - 1
    
f_final = pd.read_csv("../databases/second_part/validation_db_"+using_db+".csv", header=None)
f_final = f_final.loc[(f_final[last] == 0) | (f_final[last] == 2)]
f_final = f_final.drop(indexes, axis=1)
last_f = f_final.shape[1] - 1

f2 = f.loc[(f[last] == 0) | (f[last] == 2)]
f2 = f2.drop(indexes, axis=1)
last_f2 = f2.shape[1] - 1

X = StandardScaler().fit_transform(f2.values[:, 0:last_f2])
y = f2.values[:, last_f2]

xValidation = StandardScaler().fit_transform(f_final.values[:, 0:last_f])
yValidation = f_final.values[:, last_f]
    
#POLY -------------------------------------------------------------------------
if 0 in using_clf:
    #Entrenamiento local
    clf2 = svm.SVC(kernel='poly', degree=3)
    clf2.fit(X, y)
    
    #Pruebas
    predictions = clf2.predict(xValidation)
    cm = confusion_matrix(yValidation, predictions)
    accuracy = accuracy_score(yValidation, predictions)
    show_results(cm, accuracy, 2)

#RBF --------------------------------------------------------------------------
if 1 in using_clf:
    #Entrenamiento local
    clf2 = svm.SVC(kernel='rbf', gamma=0.15)
    clf2.fit(X, y)
    
    #Pruebas
    predictions = clf2.predict(xValidation)
    cm = confusion_matrix(yValidation, predictions)
    accuracy = accuracy_score(yValidation, predictions)
    show_results(cm, accuracy, 2)

#LINEAR------------------------------------------------------------------------
if 2 in using_clf:
    #Entrenamiento local
    clf2 = svm.SVC(kernel='linear')
    clf2.fit(X, y)
    
    #Pruebas
    predictions = clf2.predict(xValidation)
    cm = confusion_matrix(yValidation, predictions)
    accuracy = accuracy_score(yValidation, predictions)
    show_results(cm, accuracy, 2)
    
#CNN --------------------------------------------------------------------------
if 3 in using_clf:
    #Creamos el clasificador y se particiona la base de datos de entrenamiento
    X = f2.values[:, :last_f2]
    y = f2.values[:, last_f2]
    clf = MLPClassifier(activation='relu', 
                        hidden_layer_sizes=(250), 
                        early_stopping = True, 
                        validation_fraction = 0.20)
    X, X_val, y, y_val = train_test_split(X, 
                                          np.ravel(y), 
                                          random_state=np.random, 
                                          test_size=clf.validation_fraction)
    clf.fit(X, np.ravel(y))
    
    #Clasificamos todos los ejemplos de X_val
    y_pred = clf.predict(X_val)
    
    #Medidas de rendimiento
    cm = confusion_matrix(y_val, y_pred)
    acc = accuracy_score(y_val, y_pred)
    
    show_results(cm, acc, 2)
    
    filename = 'finalized_model.sav'
    pickle.dump(clf, open(filename, 'wb'))

#BAYES ------------------------------------------------------------------------
if 4 in using_clf:
    gnb = GaussianNB()
    y_pred = gnb.fit(X, y).predict(xValidation)
    
    #Medidas de rendimiento
    cm = confusion_matrix(yValidation, y_pred)
    acc = accuracy_score(yValidation, y_pred)    
    show_results(cm, acc, 2)    
