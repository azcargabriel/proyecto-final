from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split

def show_results(cm, accuracy, r):    
    fig, ax = plt.subplots()    
    ax.matshow(cm, cmap=plt.cm.Wistia)
    
    for i in range(r):
        for j in range(r):
            c = cm[j,i]
            ax.text(i, j, str(c), va='center', ha='center')
            
    print("\nAccuracy Gamma: " + str(accuracy)+"\n\n")

#Indices de las caracteristicas a quitar para la segunda clasificacion
indexes = [11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,35,36,37,38,39,40,41]

#Clasificador final
def classify(global_clf, local_clf2, x):
    ans = global_clf.predict(x)
    
    if(ans == 0 or ans == 2):
        x = np.delete(x, indexes)
        ans = local_clf2.predict([x])

    return ans

using_db = 'fragmented_database'

f = pd.read_csv("../databases/second_part/training_db_"+using_db+".csv", header=None) 
last = f.shape[1] - 1

f2 = f.loc[(f[last] == 0) | (f[last] == 2)]
f2 = f2.drop(indexes, axis=1)
last_f2 = f2.shape[1] - 1

#Entrenamiento global
X = StandardScaler().fit_transform(f.values[:, 0:last])
y = f.values[:, last]
clf = svm.SVC(kernel='linear')
clf.fit(X, y)

#Entrenamiento local 1 2
X = StandardScaler().fit_transform(f2.values[:, 0:last_f2])
y = f2.values[:, last_f2]
clf2 = svm.SVC(kernel='rbf', gamma=0.15)
clf2.fit(X, y)

#Pruebas (SVM Lineal)
f_final = pd.read_csv("../databases/second_part/test_db_"+using_db+".csv", header=None)
xValidation = StandardScaler().fit_transform(f_final.values[:, 0:last])
yValidation = f_final.values[:, last]
predictions = clf.predict(xValidation)
cm = confusion_matrix(yValidation, predictions)
accuracy = accuracy_score(yValidation, predictions)
show_results(cm, accuracy, 9)

#Pruebas (cascada)
f_final = pd.read_csv("../databases/second_part/test_db_"+using_db+".csv", header=None)
xValidation = StandardScaler().fit_transform(f_final.values[:, 0:last])
yValidation = f_final.values[:, last]
predictions = []
for x in xValidation:
    predictions.append(classify(clf, clf2, [x]))
cm = confusion_matrix(yValidation, predictions)
accuracy = accuracy_score(yValidation, predictions)
show_results(cm, accuracy, 9)
