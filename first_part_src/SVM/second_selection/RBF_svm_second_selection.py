from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score
import pandas as pd
import matplotlib.pyplot as plt

for g in [0.001,0.01,0.1,0.2,0.8,1]:
        
    #Entrenamiento
    f = pd.read_csv("../../../databases/first_part/svm_training_db_second_selection.csv", header=None)
    X = StandardScaler().fit_transform(f.values[:, 0:19])
    y = f.values[:, 19]
    clf = svm.SVC(kernel='rbf', degree=g)
    clf.fit(X, y)
    
    f2 = pd.read_csv("../../../databases/first_part/svm_validation_db_second_selection.csv", header=None)
    xValidation = StandardScaler().fit_transform(f2.values[:, 0:19])
    yValidation = f2.values[:, 19]
    predictions = clf.predict(xValidation)
    
    #Rendimiento
    cm = confusion_matrix(yValidation, predictions)
    accuracy = accuracy_score(yValidation, predictions)
    
    print("Confusion Matrix Gamma " + str(g)+":")
    print(cm)
    print("\nAccuracy Gamma "+ str(g)+": " + str(accuracy)+"\n\n")

    fig, ax = plt.subplots()    
    ax.matshow(cm, cmap=plt.cm.Wistia)
    
    for i in range(9):
        for j in range(9):
            c = cm[j,i]
            ax.text(i, j, str(c), va='center', ha='center')
        
    plt.savefig('imgs/RBF_svm_second_selection_'+str(g)+'.png', format='png', dpi=1000)
