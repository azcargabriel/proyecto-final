from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score
import pandas as pd
import matplotlib.pyplot as plt

#Entrenamiento
f = pd.read_csv("../../../databases/first_part/svm_training_db_second_selection.csv", header=None)
X = StandardScaler().fit_transform(f.values[:, 0:19])
y = f.values[:, 19]
clf = svm.SVC(kernel='linear')
clf.fit(X, y)

f2 = pd.read_csv("../../../databases/first_part/svm_validation_db_second_selection.csv", header=None)
xValidation = StandardScaler().fit_transform(f2.values[:, 0:19])
yValidation = f2.values[:, 19]
predictions = clf.predict(xValidation)

#Rendimiento
cm = confusion_matrix(yValidation, predictions)
accuracy = accuracy_score(yValidation, predictions)

print("Confusion Matrix:")
print(cm)
print("\nAccuracy: " + str(accuracy))

ig, ax = plt.subplots()    
ax.matshow(cm, cmap=plt.cm.Wistia)

for i in range(9):
    for j in range(9):
        c = cm[j,i]
        ax.text(i, j, str(c), va='center', ha='center')
    
plt.savefig('imgs/lineal_svm_second_selection.png', format='png', dpi=1000)
