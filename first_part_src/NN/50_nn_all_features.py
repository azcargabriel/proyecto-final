import numpy as np
import pandas as pd

from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

#Datos
f = pd.read_csv("../../databases/first_part/nn_training_db.csv", header=None)
X = f.values[:, :36]
y = f.values[:, 36]

#Creamos el clasificador y se particiona la base de datos de entrenamiento
clf = MLPClassifier(activation='logistic', 
                    hidden_layer_sizes=(10), 
                    early_stopping = True, 
                    validation_fraction = 0.20)
X, X_val, y, y_val = train_test_split(X, 
                                      np.ravel(y), 
                                      random_state=np.random, 
                                      test_size=clf.validation_fraction)
clf.fit(X, np.ravel(y))

#Clasificamos todos los ejemplos de X_val
y_pred = clf.predict(X_val)

#Medidas de rendimiento
cm = confusion_matrix(y_val, y_pred)
acc = accuracy_score(y_val, y_pred)

print(cm)
print(acc)

fig, ax = plt.subplots()    
ax.matshow(cm, cmap=plt.cm.Wistia)

for i in range(9):
    for j in range(9):
        c = cm[j,i]
        ax.text(i, j, str(c), va='center', ha='center')
        
plt.savefig('imgs/nn_all_features.png', format='png', dpi=1000)