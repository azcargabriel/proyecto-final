# -*- coding: utf-8 -*-

import pandas as pd
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt

f = pd.read_csv("../../databases/first_part/nn_training_db_first_selection.csv", header=None)
X = f.values[:, :15]
y = f.values[:, 15]

f = pd.read_csv("../../databases/first_part/nn_test_db_first_selection.csv", header=None)
X_v = f.values[:, :15]
y_v = f.values[:, 15]

gnb = GaussianNB()
y_pred = gnb.fit(X, y).predict(X_v)

#Medidas de rendimiento
cm = confusion_matrix(y_v, y_pred)
acc = accuracy_score(y_v, y_pred)

print("Confusion Matrix:")
print(cm)
print("\nAccuracy: " + str(acc))

fig, ax = plt.subplots()    
ax.matshow(cm, cmap=plt.cm.Wistia)

for i in range(9):
    for j in range(9):
        c = cm[j,i]
        ax.text(i, j, str(c), va='center', ha='center')
        
plt.savefig('imgs/cm_naive_bayes_first_selection.png', format='png', dpi=1000)