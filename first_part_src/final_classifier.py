from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score
import pandas as pd
import matplotlib.pyplot as plt

def show_results(cm, accuracy, r):    
    fig, ax = plt.subplots()    
    ax.matshow(cm, cmap=plt.cm.Wistia)
    
    for i in range(r):
        for j in range(r):
            c = cm[j,i]
            ax.text(i, j, str(c), va='center', ha='center')
            
    print("\nAccuracy Gamma: " + str(accuracy)+"\n\n")
    
def predict(c, c1, c2, v):
    ans = int(c.predict(v))
    if ans == 0 or ans == 1 or ans == 2:
        return c1.predict(v)
    if ans == 8 or ans == 9:
        return c2.predict(v)
    else:
        return ans
    
def predict_all(c, c1, c2, values):
    ans = []
    for v in values:
        ans.append(int(predict(c, c1, c2, v.reshape(1, -1))))
    return ans


#Entrenamiento
f = pd.read_csv("../databases/first_part/nn_training_db.csv", header=None)
X = StandardScaler().fit_transform(f.values[:, 0:36])
y = f.values[:, 36]
clf = svm.SVC(kernel='linear')
clf.fit(X, y)
#------------------------------------------------------------------------------
f_first_macro = pd.read_csv("../databases/first_part/training_db_first_macro.csv", header=None)
X = StandardScaler().fit_transform(f_first_macro.values[:, 0:36])
y = f_first_macro.values[:, 36]
clf_first_macro = svm.SVC(kernel='linear')
clf_first_macro.fit(X, y)
#------------------------------------------------------------------------------
f_second_macro = pd.read_csv("../databases/first_part/training_db_second_macro.csv", header=None)
X = StandardScaler().fit_transform(f_second_macro.values[:, 0:36])
y = f_second_macro.values[:, 36]
clf_second_macro = svm.SVC(kernel='linear')
clf_second_macro.fit(X, y)

#Rendimientos
f2 = pd.read_csv("../databases/first_part/nn_test_db.csv", header=None)
xValidation = StandardScaler().fit_transform(f2.values[:, 0:36])
yValidation = f2.values[:, 36]
predictions = clf.predict(xValidation)
cm = confusion_matrix(yValidation, predictions)
accuracy = accuracy_score(yValidation, predictions)
show_results(cm, accuracy, 9)
#------------------------------------------------------------------------------
f2 = pd.read_csv("../databases/first_part/test_db_classes_first_macro.csv", header=None)
xValidation = StandardScaler().fit_transform(f2.values[:, 0:36])
yValidation = f2.values[:, 36]
predictions = clf_first_macro.predict(xValidation)
cm = confusion_matrix(yValidation, predictions)
accuracy = accuracy_score(yValidation, predictions)
show_results(cm, accuracy, 3)
#------------------------------------------------------------------------------
f2 = pd.read_csv("../databases/first_part/test_db_classes_second_macro.csv", header=None)
xValidation = StandardScaler().fit_transform(f2.values[:, 0:36])
yValidation = f2.values[:, 36]
predictions = clf_second_macro.predict(xValidation)
cm = confusion_matrix(yValidation, predictions)
accuracy = accuracy_score(yValidation, predictions)
show_results(cm, accuracy, 2)

#Pruebas
f_final = pd.read_csv("../databases/first_part/nn_test_db.csv", header=None)
xValidation = StandardScaler().fit_transform(f_final.values[:, 0:36])
yValidation = f_final.values[:, 36]
predictions = predict_all(clf, clf_first_macro, clf_second_macro, xValidation)
cm = confusion_matrix(yValidation, predictions)
accuracy = accuracy_score(yValidation, predictions)
show_results(cm, accuracy, 9)