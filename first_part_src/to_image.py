# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

v_min = -30
v_max = 30


f = pd.read_csv("../databases/database.csv", header=None)
number_examples = len(f.index)
i = 0
while(i < number_examples):
    y = np.array(f.values[i, :36])
    y.resize((6, 6))
    plt.imsave("../databases/image_database/"+str(i)+"_"+str(int(f.values[i, 36]))+".png", y, vmin=v_min, vmax=v_max)
    i = i + 1
